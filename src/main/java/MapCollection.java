import java.util.List;
import java.util.stream.Collectors;

public class MapCollection {
    
    public static List<Integer> doubleCollection(List<Integer> list) {
        // // Need to be implemented
        return list.stream().map(t -> t * 2).collect(Collectors.toList());
    }
    
    public static List<String> mapToStringCollection(List<Integer> list) {
        // Need to be implemented
        return list.stream().map(t -> String.valueOf((char)(t + 96))).collect(Collectors.toList());
    }
    
    public static List<String> uppercaseCollection(List<String> list) {
        // Need to be implemented
        return list.stream().map(t -> t.toUpperCase()).collect(Collectors.toList());
    }
    
    public static List<Integer> transformTwoDimensionalToOne(List<List<Integer>> list) {
        // Need to be implemented
        return list.stream().flatMap(t -> t.stream()).collect(Collectors.toList());
    }
}
