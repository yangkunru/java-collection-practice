import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.lang.Math.abs;

public class CollectionOperation {
    
    public static List<Integer> getListByInterval(int left, int right) {
        // Need to be implemented
        return Stream.iterate(left, n -> left < right? ++n : --n).limit(abs(left-right)+1).collect(Collectors.toList());
    }
    
    
    public static List<Integer> removeLastElement(List<Integer> list) {
        // Need to be implemented
        return list.stream().limit(list.size()-1).collect(Collectors.toList());
    }
    
    public static List<Integer> sortDesc(List<Integer> list) {
        // Need to be implemented
        return list.stream().sorted((x, y) -> y - x).collect(Collectors.toList());
    }
    
    
    public static List<Integer> reverseList(List<Integer> list) {
        // Need to be implemented
        Collections.reverse(list);
        return list;
    }
    
    
    public static List<Integer> concat(List<Integer> list1, List<Integer> list2) {
        // Need to be implemented
        return Stream.concat(list1.stream(), list2.stream()).collect(Collectors.toList());
    }
    
    public static List<Integer> union(List<Integer> list1, List<Integer> list2) {
        // Need to be implemented
        return Stream.concat(list1.stream(), list2.stream()).distinct().collect(Collectors.toList());
    }
    
    public static boolean isAllElementsEqual(List<Integer> list1, List<Integer> list2) {
        // Need to be implemented
        return list1.stream().allMatch(element -> list2.contains(element)) && list2.stream().allMatch(element -> list1.contains(element));
    }
}
