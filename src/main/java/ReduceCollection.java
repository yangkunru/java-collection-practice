import java.util.List;
import java.util.stream.Collectors;

public class ReduceCollection {
    public static int getMax(List<Integer> list) {
        // Need to be implemented
        return list.stream().reduce(list.get(0), (a, b) -> a > b ? a : b);
    }
    
    public static double getAverage(List<Integer> list) {
        // Need to be implemented
        return list.stream().mapToDouble(x -> x).reduce(0, (a, b) -> a + b)/list.size();
    }
    
    public static int getSum(List<Integer> list) {
        // Need to be implemented
        return list.stream().reduce(0, (a, b) -> a + b);
    }
    
    public static double getMedian(List<Integer> list) {
        // Need to be implemented
        return list.stream().mapToDouble(x -> x).sorted().skip(list.size() / 2 - (list.size() % 2 == 0 ? 1 : 0)).limit(list.size() % 2 == 0 ? 2 : 1).average().getAsDouble();
    }
    
    public static int getFirstEven(List<Integer> list) {
        // Need to be implemented
        return list.stream().filter(t -> t % 2 == 0).collect(Collectors.toList()).get(0);
    }
}
