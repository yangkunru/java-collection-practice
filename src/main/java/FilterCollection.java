import java.util.List;
import java.util.stream.Collectors;

public class FilterCollection {
    
    public static List<Integer> getAllEvens(List<Integer> list) {
        // Need to be implemented
        return list.stream().filter(t -> t%2 == 0).collect(Collectors.toList());
    }
    
    public static List<Integer> removeDuplicateElements(List<Integer> list) {
        // Need to be implemented
        return list.stream().distinct().collect(Collectors.toList());
    }
    
    public static List<Integer> getCommonElements(List<Integer> collection1, List<Integer> collection2) {
        // Need to be implemented
        return collection1.stream().filter(t -> collection2.contains(t)).collect(Collectors.toList());
    }
}
